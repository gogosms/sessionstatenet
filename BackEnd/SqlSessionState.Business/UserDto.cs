﻿using System;

namespace SqlSessionState.Business
{
	[Serializable]
	public class UserDto
	{
		public string Cod { get; set; }
		
		public string Name { get; set; }
		
		public string LastName { get; set; }

		public string Dni { get; set; }

		public override string ToString()
		{
			return $"{Cod}-{Name} {LastName} (Dni: {Dni})";
		}
	}
}
