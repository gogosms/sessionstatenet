﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SqlSessionState.Business;

namespace SqlSessionState.WebApp1.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			var userDTo = new UserDto {Cod = "001", Name = "Matias", LastName = "Katz", Dni = "33675984"};
			Session["User"] = userDTo;
			ViewBag.User = Session["User"];
			return View();
		}
		
	}
}