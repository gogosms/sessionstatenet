﻿using System.Web.Mvc;
using SqlSessionState.Business;

namespace SqlSessionState.WebApp2.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			ViewBag.User = (UserDto)Session["User"];
			return View();
		}

		
	}
}